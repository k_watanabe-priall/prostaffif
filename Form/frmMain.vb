﻿Imports Npgsql

Public Class frmMain
    Private aryGetStaff() As TYPE_GET_STAFF
    Private typUpdStaff As TYPE_UPD_STAFF
    Private typProfile As TYPE_UPD_PROFILE
    Private typWorkCareer As TYPE_UPD_WORKCAREER

#Region "Form Events"
    ''' <summary>
    ''' FormLoad
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        '---タイトル表示
        Me.Text = "職員情報連携アプリケーション" & Space(1) &
                  "Ver." & My.Application.Info.Version.Major &
                  "." & My.Application.Info.Version.Minor &
                  "." & My.Application.Info.Version.Build

        '---リストボックスの初期化
        Me.lstMsg.Items.Clear()

        '---リストボックス表示
        Call subDispList(0, "職員情報連携アプリケーション 起動", strMethod)

        '---ログ出力
        Call subOutLog(0, "職員情報連携アプリケーション 起動", strMethod)

        Me.tmrProc.Interval = My.Settings.Interval
        Me.tmrProc.Enabled = True
    End Sub
#End Region

#Region "ControlEvents"
    ''' <summary>
    ''' 閉じるボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        '---ログ出力
        Call subOutLog(0, "職員情報連携アプリケーション 終了", strMethod)

        End
    End Sub

    ''' <summary>
    ''' 処理タイマー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub tmrProc_Tick(sender As Object, e As EventArgs) Handles tmrProc.Tick
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Me.tmrProc.Enabled = False

        If fncGetStaff() = RET_ERROR Then
            '---リストボックス表示
            Call subDispList(RET_ERROR, "職員情報取得エラー", strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "職員情報登録/更新エラー：ユーザID=" & typUpdStaff.userid, strMethod)
        End If

        End

        Me.tmrProc.Enabled = True
    End Sub
#End Region

#Region "SubRoutine"

    ''' <summary>
    ''' リストボックス表示
    ''' </summary>
    ''' <param name="Proc">0・・・Inf、2・・・War、-9・・・Err</param>
    ''' <param name="msg">出力メッセージ</param>
    ''' <param name="strMethod">発生場所</param>
    Private Sub subDispList(ByVal Proc As Integer, ByVal msg As String, ByVal strMethod As String)
        Dim strOutMsg As String = vbNullString
        Dim strYYYY As String = Now.Year.ToString
        Dim strMM As String = Now.Month.ToString("0#")
        Dim strDD As String = Now.Day.ToString("0#")
        Dim strDateTime As String = Now.ToString("yyyy/MM/dd HH:mm:ss")

        Select Case Proc
            Case RET_NORMAL
                strOutMsg = "[INF](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
            Case RET_WARNING
                strOutMsg = "[WAR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
            Case RET_ERROR
                strOutMsg = "[ERR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
        End Select

        If Me.lstMsg.Items.Count > My.Settings.MaxListRow Then
            Me.lstMsg.Items.Clear()
        End If

        Me.lstMsg.Items.Add(strOutMsg)

        Me.lstMsg.SelectedIndex = Me.lstMsg.Items.Count - 1

        Me.lstMsg.Refresh()
    End Sub

    ''' <summary>
    ''' ログとリストボックス両方にメッセージを出力
    ''' </summary>
    ''' <param name="Proc"></param>
    ''' <param name="msg"></param>
    ''' <param name="strMethod"></param>
    Private Sub subOutLogList(ByVal Proc As Integer, ByVal msg As String, ByVal strMethod As String)

        Call subDispList(Proc, msg, strMethod)
        Call subOutLog(Proc, msg, strMethod)
    End Sub

    ''' <summary>
    ''' 職員情報取得/登録/更新処理
    ''' </summary>
    ''' <returns></returns>
    Private Function fncGetStaff() As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim dt As New DataTable
        Dim intCounter As Integer = 0

        Dim objGroup As New d_prim_manurtyDataSetTableAdapters.GroupsTableAdapter
        Dim dtGroup As New d_prim_manurtyDataSet.GroupsDataTable
        Dim objOccupation As New d_prim_manurtyDataSetTableAdapters.OccupationsTableAdapter
        Dim dtOccupation As New d_prim_manurtyDataSet.OccupationsDataTable
        Dim objPositions As New d_prim_manurtyDataSetTableAdapters.PositionsTableAdapter
        Dim dtPositions As New d_prim_manurtyDataSet.PositionsDataTable
        Dim objProfile As New d_prim_manurtyDataSetTableAdapters.UserProfilesTableAdapter
        Dim dtProfile As New d_prim_manurtyDataSet.UserProfilesDataTable
        Dim objUser As New d_prim_manurtyDataSetTableAdapters.UsersTableAdapter
        Dim dtUser As New d_prim_manurtyDataSet.UsersDataTable
        Dim intProcInterval As Integer = 0

        Dim strUserId As String = vbNullString

        Try
            '---リストボックス表示
            Call subDispList(0, "----- 職員情報取得(Oracle) -----", strMethod)
            '---ログ出力
            Call subOutLog(0, "----- 職員情報取得(Oracle) -----", strMethod)

            '---処理間隔の退避
            intProcInterval = My.Settings.ProcInterval

            '---SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT")
            strSQL.AppendLine("NVL(STAFFCODE,'') AS STAFFCODE,")
            'strSQL.AppendLine("GENERATIONNO,")
            strSQL.AppendLine("NVL(WARDCODE,'') AS WARDCODE,")
            strSQL.AppendLine("NVL(PROFESSIONCODE,'') AS PROFESSIONCODE,")
            strSQL.AppendLine("NVL(DEGREECODE,'') AS DEGREECODE,")
            strSQL.AppendLine("NVL(KANANAME,'') AS KANANAME,")
            strSQL.AppendLine("NVL(KANJINAME,'') AS KANJINAME,")
            strSQL.AppendLine("NVL(PASSWORD,'') AS PASSWORD,")
            strSQL.AppendLine("VALIDSTARTDATE,")
            strSQL.AppendLine("VALIDENDDATE,")
            strSQL.AppendLine("SELECTSTAFFSORTKEY")
            strSQL.AppendLine("FROM MSCOMSTAFF")
            strSQL.AppendLine("WHERE ((VALIDSTARTDATE <= (SELECT SYSDATE FROM DUAL))")
            strSQL.AppendLine("AND (VALIDENDDATE >= (SELECT SYSDATE FROM DUAL)))")

            If intProcInterval > 0 Then
                strSQL.AppendLine("AND ((IF_CREATEDATE >= (SELECT SYSDATE - " & intProcInterval & "/24 FROM DUAL))")
                strSQL.AppendLine("OR (IF_UPDATEDATE >= (SELECT SYSDATE - " & intProcInterval & "/24 FROM DUAL)))")
            Else
                '---ProcIntervalが0以下は全データ対象とする。
            End If

            '---データ取得
            If DBQueryOracle(strSQL.ToString, dt) = RET_ERROR Then
                Call subDispList(RET_ERROR, "職員情報検索(Oracle)エラー", strMethod)
                Return RET_ERROR
            End If
            '---取得結果0件
            If dt.Rows.Count = 0 Then
                Call subDispList(RET_NORMAL, "対象職員情報(Oracle)はありませんでした。", strMethod)
                Return RET_NOTFOUND
            Else
                Call subDispList(RET_NORMAL, "対象職員情報(Oracle)取得件数：" & dt.Rows.Count & "件", strMethod)
            End If
            '---構造体の初期化
            Erase aryGetStaff

            '---取得データ構造体退避
            For intCounter = 0 To dt.Rows.Count - 1
                System.Windows.Forms.Application.DoEvents()

                ReDim Preserve aryGetStaff(intCounter)

                '---取得データの構造体退避
                With aryGetStaff(intCounter)
                    .STAFFCODE = dt.Rows(intCounter).Item("STAFFCODE").ToString                                 '職員番号
                    .GENERATIONNO = "0"                                                                         '世代番号 0固定
                    .WARDCODE = dt.Rows(intCounter).Item("WARDCODE").ToString                                   '病棟コード
                    .PROFESSIONCODE = dt.Rows(intCounter).Item("PROFESSIONCODE").ToString                       '職種コード
                    .DEGREECODE = dt.Rows(intCounter).Item("DEGREECODE").ToString                               '役職コード
                    .KANANAME = dt.Rows(intCounter).Item("KANANAME").ToString                                   'カナ氏名
                    .KANJINAME = dt.Rows(intCounter).Item("KANJINAME").ToString                                 '漢字氏名
                    .PASSWORD = dt.Rows(intCounter).Item("PASSWORD").ToString                                   'パスワード 
                    .VALIDSTARTDATE = dt.Rows(intCounter).Item("VALIDSTARTDATE").ToString                       '有効開始日
                    .VALIDENDDATE = dt.Rows(intCounter).Item("VALIDENDDATE").ToString                           '有効終了日
                    .LICENSECODE1 = ""                                                                          '免許コード1
                    .LICENSEDATE1 = ""                                                                          '免許日付1
                    .LICENSECODE2 = ""                                                                          '免許コード2
                    .LICENSEDATE2 = ""                                                                          '免許日付2
                    .SPECIALFLG = ""                                                                            '指定医フラグ※精神科
                    .PWDUPDATEDATE = ""                                                                         'パスワード更新日
                    .ENCRYPT = ""                                                                               '暗号化状態
                    .ADVISINGDOCTOR = ""                                                                        '指導医□
                    .SELECTSTAFFSORTKEY = dt.Rows(intCounter).Item("SELECTSTAFFSORTKEY").ToString               '職員選択リストソートキー
                    .WINDOWSUSERNAME = ""                                                                       'Windowsログインユーザ
                End With

                '---更新データ構造体退避
                typUpdStaff = New TYPE_UPD_STAFF

                With typUpdStaff
                    .userid = aryGetStaff(intCounter).STAFFCODE
                    .name = aryGetStaff(intCounter).KANJINAME.Trim
                    '---カナ氏名を全角に変換 Ver.1.0.3 UpDated By Watanabe 2020.12.28
                    '.nameKana = aryGetStaff(intCounter).KANANAME.Trim
                    If aryGetStaff(intCounter).KANANAME.Trim <> "" Then
                        .nameKana = StrConv(aryGetStaff(intCounter).KANANAME.Trim, VbStrConv.Wide)
                    End If
                    .password = aryGetStaff(intCounter).PASSWORD.Trim
                End With
                Call subDispList(RET_NORMAL, "(" & intCounter + 1 & "/" & dt.Rows.Count & ") 職員コード:" & aryGetStaff(intCounter).STAFFCODE & ",氏名:" & aryGetStaff(intCounter).KANJINAME.Trim & "(" & aryGetStaff(intCounter).KANANAME.Trim & ")", strMethod)

                '---Userテーブル登録/更新
                If fncUpdStaff(typUpdStaff) = RET_ERROR Then
                    '---リストボックス表示 Ver.1.0.3 職員コードに変更
                    Call subDispList(RET_ERROR, "職員情報登録/更新エラー：職員コード=" & typUpdStaff.userid, strMethod)

                    '---ログ出力
                    Call subOutLog(RET_ERROR, "職員情報登録/更新エラー：職員コード=" & typUpdStaff.userid, strMethod)

                    '---エラー時はスキップ
                    Continue For
                End If

                '---プロファイル構造体退避
                typProfile = New TYPE_UPD_PROFILE
                With typProfile
                    '.UserId = fncGetUserId(aryGetStaff(intCounter).STAFFCODE)
                    objUser.FillByUserid(dtUser, aryGetStaff(intCounter).STAFFCODE)
                    If dtUser.Rows.Count > 0 Then
                        .UserId = dtUser.Rows(0).Item("id")
                    End If
                    .empcode = aryGetStaff(intCounter).STAFFCODE                                                '職員番号
                    '.groupcode = aryGetStaff(intCounter).WARDCODE                                               '所属
                    '---所属コードを変換し退避
                    .groupcode = fncGetConvGroups(aryGetStaff(intCounter).WARDCODE)
                    '---職種取得
                    'objOccupation.FillByExtension(dtOccupation, aryGetStaff(intCounter).PROFESSIONCODE)
                    'If dtOccupation.Rows.Count > 0 Then
                    '    .OccupationId = dtOccupation.Rows(0).Item("id")                                         '職種
                    'End If
                    '---職位コードを変換し退避
                    .OccupationId = fncGetConvOccupation(aryGetStaff(intCounter).PROFESSIONCODE)

                    '---職種取得
                    'objPositions.FillByExtension(dtPositions, aryGetStaff(intCounter).DEGREECODE)
                    'If dtPositions.Rows.Count > 0 Then
                    '    .PositionId = dtPositions.Rows(0).ItemArray("id")                                       '役職
                    'End If
                    '---職位コードを変換し退避
                    If aryGetStaff(intCounter).DEGREECODE = "" Then
                        .PositionId = "1"
                    Else
                        .PositionId = fncGetConvPosition(aryGetStaff(intCounter).DEGREECODE)
                    End If
                    .EmpTypeId = "1"                                                                            '勤務形態 1:常勤 固定
                End With

                '---UserProfileテーブル登録/更新
                If fncUpdProfile(typProfile) = RET_ERROR Then
                    '---リストボックス表示
                    Call subDispList(RET_ERROR, "職員プロファイル情報登録/更新エラー：患者ID=" & typUpdStaff.id, strMethod)

                    '---ログ出力
                    Call subOutLog(RET_ERROR, "職員プロファイル情報登録/更新エラー：患者ID=" & typUpdStaff.id, strMethod)

                    '---エラー時はスキップ
                    Continue For
                End If

                '---ワークキャリア構造体退避
                typWorkCareer = New TYPE_UPD_WORKCAREER
                With typWorkCareer
                    .started = dt.Rows(intCounter).Item("VALIDSTARTDATE").ToString                              '開始日
                    .ended = dt.Rows(intCounter).Item("VALIDENDDATE").ToString                                  '終了日
                    .isMain = "True"
                    '---Group取得
                    'objGroup.FillByExtension(dtGroup, dt.Rows(intCounter).Item("WARDCODE").ToString)
                    'If dtGroup.Rows.Count > 0 Then
                    '    .GroupId = dtGroup.Rows(0).Item("id")                                                   '所属
                    'End If
                    '---所属コードを変換し退避
                    If dt.Rows(intCounter).Item("WARDCODE").ToString = "" Then
                        .GroupId = "32"
                    Else
                        .GroupId = fncGetConvGroups(dt.Rows(intCounter).Item("WARDCODE").ToString)
                    End If

                    '---Profile取得
                    'objUser.FillByUserid(dtUser, aryGetStaff(intCounter).STAFFCODE)
                    'If dtUser.Rows.Count > 0 Then
                    '    strUserId = dtUser.Rows(0).Item("id")
                    objProfile.FillByUserId(dtProfile, CInt(typProfile.UserId))
                    If dtProfile.Rows.Count > 0 Then
                        .UserProfileId = dtProfile.Rows(0).Item("id")
                    End If
                    'End If
                    '.UserProfileId = fncGetUserProfileID(typProfile.UserId)
                    .OccupationId = typProfile.OccupationId
                    .PositionId = typProfile.PositionId
                    .EmpTypeId = "1"                                                                            '勤務形態 1:常勤 固定
                End With

                '---START Ver.1.0.6 田中さんをWorkCareer更新から除外するよう修正 ADD By Watanabe 2021.05.19
                '---右記も追加本館2F眼科の3名(ｳﾒｻﾞｷ ｱｻｺ,ｶｲﾏ ｱｽﾞｻ,ﾖｼﾑﾗ ﾁｸﾞｻ) UpDated By Watanabe 2021.05.24
                If typWorkCareer.UserProfileId = "1134" Or typWorkCareer.UserProfileId = "948" Or typWorkCareer.UserProfileId = "1010" Or typWorkCareer.UserProfileId = "1391" Then
                    Continue For
                End If
                '---END Ver.1.0.6 田中さんをWorkCareer更新から除外するよう修正 ADD By Watanabe 2021.05.19

                '---WorkCareersテーブル登録/更新
                If fncUpdWorkCareer(typWorkCareer) = RET_ERROR Then
                    '---リストボックス表示
                    Call subDispList(RET_ERROR, "ワークキャリア情報登録/更新エラー：患者ID=" & typUpdStaff.id, strMethod)

                    '---ログ出力
                    Call subOutLog(RET_ERROR, "ワークキャリア情報登録/更新エラー：患者ID=" & typUpdStaff.id, strMethod)

                    '---エラー時はスキップ
                    Continue For
                End If

            Next intCounter

            Return RET_NORMAL

        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally

        End Try
    End Function

    ''' <summary>
    ''' 職員情報登録/更新
    ''' </summary>
    ''' <returns></returns>
    Private Function fncUpdStaff(ByVal objStaff As TYPE_UPD_STAFF) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim objData As New d_prim_manurtyDataSetTableAdapters.UsersTableAdapter
        Dim dt As New d_prim_manurtyDataSet.UsersDataTable
        Dim strSQL As New System.Text.StringBuilder
        Dim cmd As NpgsqlCommand

        Try
            '---存在チェック
            objData.FillByUserid(dt, objStaff.userid)

            strSQL.Clear()

            If dt.Rows.Count = 0 Then
                strSQL.AppendLine("INSERT INTO ""Users""")
                strSQL.AppendLine("(")
                strSQL.AppendLine("""userid"",")
                strSQL.AppendLine("""password"",")
                strSQL.AppendLine("""name"",")
                strSQL.AppendLine("""nameKana"",")
                strSQL.AppendLine("""createdAt"",")
                strSQL.AppendLine("""updatedAt"",")
                strSQL.AppendLine("""MenuId"",")
                strSQL.AppendLine("""MenuTopId""")
                strSQL.AppendLine(")")
                strSQL.AppendLine("VALUES")
                strSQL.AppendLine("(")
                strSQL.AppendLine("'" & objStaff.userid & "',")
                strSQL.AppendLine("'" & objStaff.password & "',")
                strSQL.AppendLine("'" & objStaff.name & "',")
                strSQL.AppendLine("'" & objStaff.nameKana & "',")
                strSQL.AppendLine("'" & Date.Now & "',")
                strSQL.AppendLine("'" & Date.Now & "',")
                strSQL.AppendLine("3,")
                strSQL.AppendLine("3")
                strSQL.AppendLine(")")
            Else
                strSQL.AppendLine("UPDATE ""Users""")
                strSQL.AppendLine("SET")
                strSQL.AppendLine("""name"" = '" & objStaff.name & "',")
                strSQL.AppendLine("""nameKana"" = '" & objStaff.nameKana & "',")
                strSQL.AppendLine("""password"" = '" & objStaff.password & "',")
                strSQL.AppendLine("""updatedAt"" = '" & Date.Now & "'")
                strSQL.AppendLine("WHERE ")
                strSQL.AppendLine("""id"" = " & dt.Rows(0).Item("id"))
            End If

            '---生成SQL文をログ出力
            Call subOutLog(RET_NORMAL, "SQL：" & strSQL.ToString, strMethod)

            '---Postgret接続
            Call ConnectionOpenPostgret()

            cmd = New NpgsqlCommand(strSQL.ToString, connPostgret)
            cmd.ExecuteNonQuery()

            cmd.Dispose()
            objData.Dispose()
            dt.Dispose()

            '---Postgret切断
            Call ConnectionClosePostgret()

            Return RET_NORMAL
        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally

        End Try
    End Function

    ''' <summary>
    ''' 職員プロファイル情報登録/更新
    ''' </summary>
    ''' <param name="objProfile">プロファイル更新用構造体</param>
    ''' <returns></returns>
    Private Function fncUpdProfile(ByVal objProfile As TYPE_UPD_PROFILE) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim objData As New d_prim_manurtyDataSetTableAdapters.UserProfilesTableAdapter
        Dim dt As New d_prim_manurtyDataSet.UserProfilesDataTable
        Dim strSQL As New System.Text.StringBuilder
        Dim cmd As NpgsqlCommand
        Dim objDate As DateTime = DateTime.Now

        Try
            '---存在チェック
            objData.FillByUserId(dt, CInt(objProfile.UserId))

            strSQL.Clear()

            If dt.Rows.Count = 0 Then
                strSQL.AppendLine("INSERT INTO ""UserProfiles""")
                strSQL.AppendLine("(")
                strSQL.AppendLine("""empcode"",")
                strSQL.AppendLine("""groupcode"",")
                strSQL.AppendLine("""createdAt"",")
                strSQL.AppendLine("""updatedAt"",")
                strSQL.AppendLine("""UserId"",")
                strSQL.AppendLine("""OccupationId"",")
                strSQL.AppendLine("""PositionId"",")
                strSQL.AppendLine("""EmpTypeId""")
                strSQL.AppendLine(")")
                strSQL.AppendLine("VALUES")
                strSQL.AppendLine("(")
                strSQL.AppendLine("'" & objProfile.empcode & "',")
                strSQL.AppendLine("'" & objProfile.groupcode & "',")
                strSQL.AppendLine("'" & objDate & "',")
                strSQL.AppendLine("'" & objDate & "',")
                strSQL.AppendLine("" & objProfile.UserId & ",")
                strSQL.AppendLine("" & objProfile.OccupationId & ",")
                strSQL.AppendLine("" & objProfile.PositionId & ",")
                strSQL.AppendLine("" & objProfile.EmpTypeId & "")
                strSQL.AppendLine(")")
            Else
                strSQL.AppendLine("UPDATE ""UserProfiles""")
                strSQL.AppendLine("SET")
                strSQL.AppendLine("""empcode"" = '" & objProfile.empcode & "',")

                '---START Ver.1.0.6 田中さん以外の場合、groupcodeを更新する。 ADD By Watanabe 2021.05.19
                If dt.Rows(0).Item("id").ToString <> "1134" Then
                    strSQL.AppendLine("""groupcode"" = '" & objProfile.groupcode & "',")
                End If
                '---END Ver.1.0.6 田中さん以外の場合、groupcodeを更新する。 ADD By Watanabe 2021.05.19

                strSQL.AppendLine("""updatedAt"" = '" & objDate & "',")
                strSQL.AppendLine("""OccupationId"" = '" & objProfile.OccupationId & "',")
                strSQL.AppendLine("""PositionId"" = '" & objProfile.PositionId & "'")
                strSQL.AppendLine("WHERE")
                strSQL.AppendLine("""id"" = " & dt.Rows(0).Item("id"))
            End If

                '---生成SQL文をログ出力
                Call subOutLog(RET_NORMAL, "SQL：" & strSQL.ToString, strMethod)

            '---Postgret接続
            Call ConnectionOpenPostgret()

            cmd = New NpgsqlCommand(strSQL.ToString, connPostgret)
            cmd.ExecuteNonQuery()

            cmd.Dispose()
            objData.Dispose()
            dt.Dispose()

            '---Postgret切断
            Call ConnectionClosePostgret()

            '---生成SQL文をログ出力
            Call subOutLog(RET_NORMAL, "UserProfiles登録完了", strMethod)

            Return RET_NORMAL
        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally

        End Try
    End Function

    ''' <summary>
    ''' ワークキャリア情報登録/更新
    ''' </summary>
    ''' <param name="objCareer">キャリア情報更新用構造体</param>
    ''' <returns></returns>
    Private Function fncUpdWorkCareer(ByVal objCareer As TYPE_UPD_WORKCAREER) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim objData As New d_prim_manurtyDataSetTableAdapters.WorkCareersTableAdapter
        Dim dt As New d_prim_manurtyDataSet.WorkCareersDataTable
        Dim strSQL As New System.Text.StringBuilder
        Dim cmd As NpgsqlCommand
        Dim objDate As DateTime = DateTime.Now
        Dim intCounter As Integer = 0

        Try
            Call subOutLog(RET_NORMAL, "ワークキャリア情報登録/更新", strMethod)

            '---存在チェック
            'objData.FillByProfileId(dt, CInt(objCareer.UserProfileId), objDate.ToString("yyyy/MM/dd"))
            objData.FillByProfileId(dt, CInt(objCareer.UserProfileId), objDate.ToString("yyyy/MM/dd"), CInt(objCareer.GroupId), CInt(objCareer.OccupationId), CInt(objCareer.PositionId))

            strSQL.Clear()

            'キャリア情報は履歴管理するため、既存に関しては有効期限を前日に更新し、無効データとする。
            If dt.Rows.Count > 0 Then

                For intCounter = 0 To dt.Rows.Count - 1
                    strSQL.AppendLine("UPDATE ""WorkCareers""")
                    strSQL.AppendLine("SET")
                    strSQL.AppendLine("""ended""= '" & objDate.AddDays(-1) & "',")
                    strSQL.AppendLine("""isMain""= 'False',")
                    strSQL.AppendLine("""updatedAt""= '" & objDate & "'")
                    strSQL.AppendLine("WHERE ""id"" = " & dt.Rows(0).Item("id"))

                    '---生成SQL文をログ出力
                    Call subOutLog(RET_NORMAL, "SQL：" & strSQL.ToString, strMethod)

                    '---Postgret接続
                    Call ConnectionOpenPostgret()

                    cmd = New NpgsqlCommand(strSQL.ToString, connPostgret)
                    cmd.ExecuteNonQuery()

                    cmd.Dispose()
                    objData.Dispose()

                    '---Postgret切断
                    Call ConnectionClosePostgret()

                    Call subOutLog(RET_NORMAL, "WorkCareers更新完了", strMethod)
                Next intCounter
            End If

            objData.FillByGID(dt, CInt(objCareer.UserProfileId), CInt(objCareer.GroupId), CInt(objCareer.OccupationId), CInt(objCareer.PositionId), objDate.ToString("yyyy/MM/dd"))

            If dt.Rows.Count = 0 Then
                strSQL.Clear()
                strSQL.AppendLine("INSERT INTO ""WorkCareers""")
                strSQL.AppendLine("(")
                strSQL.AppendLine("""started"",")
                '---終了日を設定すると部署職種がManurtyのログイン者情報に表示されないため、終了日は設定しない。
                'strSQL.AppendLine("""ended"",")
                strSQL.AppendLine("""isMain"",")
                strSQL.AppendLine("""createdAt"",")
                strSQL.AppendLine("""updatedAt"",")
                strSQL.AppendLine("""GroupId"",")
                strSQL.AppendLine("""UserProfileId"",")
                strSQL.AppendLine("""OccupationId"",")
                strSQL.AppendLine("""PositionId"",")
                strSQL.AppendLine("""EmpTypeId""")
                strSQL.AppendLine(")")
                strSQL.AppendLine("VALUES")
                strSQL.AppendLine("(")
                strSQL.AppendLine("'" & objCareer.started & "',")
                '---終了日を設定すると部署職種がManurtyのログイン者情報に表示されないため、終了日は設定しない。
                'strSQL.AppendLine("'" & objCareer.ended & "',")
                strSQL.AppendLine("'" & objCareer.isMain & "',")
                strSQL.AppendLine("'" & objDate & "',")
                strSQL.AppendLine("'" & objDate & "',")
                strSQL.AppendLine("" & objCareer.GroupId & ",")
                strSQL.AppendLine("" & objCareer.UserProfileId & ",")
                strSQL.AppendLine("" & objCareer.OccupationId & ",")
                strSQL.AppendLine("" & objCareer.PositionId & ",")
                strSQL.AppendLine("" & objCareer.EmpTypeId & "")
                strSQL.AppendLine(")")

                '---生成SQL文をログ出力
                Call subOutLog(RET_NORMAL, "SQL：" & strSQL.ToString, strMethod)

                '---Postgret接続
                Call ConnectionOpenPostgret()

                cmd = New NpgsqlCommand(strSQL.ToString, connPostgret)
                cmd.ExecuteNonQuery()

                cmd.Dispose()
                objData.Dispose()
                dt.Dispose()

                '---Postgret切断
                Call ConnectionClosePostgret()

                Call subOutLog(RET_NORMAL, "WorkCareers登録完了", strMethod)
            End If

            Return RET_NORMAL
        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally

        End Try
    End Function

    ''' <summary>
    ''' 所属コード変換
    ''' </summary>
    ''' <param name="strCode">電子カルテ側コード</param>
    ''' <returns>Manurty側コード</returns>
    Private Function fncGetConvGroups(ByVal strCode As String) As String
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '---リストボックス表示
            Call subDispList(RET_NORMAL, "所属変換：電子カルテ所属コードID=" & strCode, strMethod)
            '---ログ出力
            Call subOutLog(RET_NORMAL, "所属変換：電子カルテ所属コードID=" & strCode, strMethod)

            strSQL.Clear()
            strSQL.AppendLine("SELECT MNT_CODE")
            strSQL.AppendLine("FROM TRN_ConvDepartment")
            strSQL.AppendLine("WHERE EM_CODE = '" & strCode & "'")

            '---SQLサーバ接続
            Call fncDBConnect()

            '---SQL発行
            dsData = fncAdptSQL(connSQLServer, strSQL.ToString)


            If dsData.Tables(0).Rows.Count = 0 Then
                Return "32"
            Else
                Return dsData.Tables(0).Rows(0).Item("MNT_CODE")
            End If

            '---SQLサーバ切断
            Call fncDBDisConnect()

        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally
            dsData.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' 職種コード変換
    ''' </summary>
    ''' <param name="strCode">電子カルテ側コード</param>
    ''' <returns>Manurty側コード</returns>
    Private Function fncGetConvOccupation(ByVal strCode As String) As String
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '---リストボックス表示
            Call subDispList(RET_NORMAL, "職種変換：電子カルテ所属コードID=" & strCode, strMethod)
            '---ログ出力
            Call subOutLog(RET_NORMAL, "職種変換：電子カルテ所属コードID=" & strCode, strMethod)

            strSQL.Clear()
            strSQL.AppendLine("SELECT MNT_CODE")
            strSQL.AppendLine("FROM TRN_ConvOccupation")
            strSQL.AppendLine("WHERE EM_CODE = '" & strCode & "'")

            '---SQLサーバ接続
            Call fncDBConnect()

            '---SQL発行
            dsData = fncAdptSQL(connSQLServer, strSQL.ToString)

            '---SQLサーバ切断
            Call fncDBDisConnect()

            If dsData.Tables(0).Rows.Count = 0 Then
                Return "27"
            Else
                Return dsData.Tables(0).Rows(0).Item("MNT_CODE")
            End If

        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally
            dsData.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' 職位コード変換
    ''' </summary>
    ''' <param name="strCode">電子カルテ側コード</param>
    ''' <returns>Manurty側コード</returns>
    Private Function fncGetConvPosition(ByVal strCode As String) As String
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '---リストボックス表示
            Call subDispList(RET_NORMAL, "職位変換：電子カルテ職位コードID=" & strCode, strMethod)
            '---ログ出力
            Call subOutLog(RET_NORMAL, "職位変換：電子カルテ職位コードID=" & strCode, strMethod)

            strSQL.Clear()
            strSQL.AppendLine("SELECT MNT_CODE")
            strSQL.AppendLine("FROM TRN_ConvPosition")
            strSQL.AppendLine("WHERE EM_CODE = '" & strCode & "'")

            '---SQLサーバ接続
            Call fncDBConnect()

            '---SQL発行
            dsData = fncAdptSQL(connSQLServer, strSQL.ToString)

            '---SQLサーバ切断
            Call fncDBDisConnect()

            If dsData.Tables(0).Rows.Count = 0 Then
                Return "1"
            Else
                Return dsData.Tables(0).Rows(0).Item("MNT_CODE")
            End If

        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally
            dsData.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Usersのid取得
    ''' </summary>
    ''' <param name="strUserId">ユーザID</param>
    ''' <returns></returns>
    Private Function fncGetUserId(ByVal strUserId As String) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim cmd As NpgsqlCommand
        Dim dr As NpgsqlDataReader

        Try
            '---リストボックス表示
            Call subDispList(RET_NORMAL, "プロファイル用userid取得：Users.userid = " & strUserId, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "プロファイル用userid取得：Users.userid = " & strUserId, strMethod)

            strSQL.Clear()
            strSQL.AppendLine("SELECT ""id""")
            strSQL.AppendLine("FROM ""Users""")
            strSQL.AppendLine("WHERE ""userid"" = '" & strUserId & "'")

            '---Postgret接続
            Call ConnectionOpenPostgret()

            '---SQL発行
            cmd = New NpgsqlCommand(strSQL.ToString, connPostgret)
            dr = cmd.ExecuteReader

            If dr.HasRows Then
                '---取得データ退避
                While dr.Read()
                    Return dr("id")
                End While
            Else
                Return RET_NOTFOUND
            End If

            dr.Close()

            '---SQLサーバ切断
            Call ConnectionClosePostgret()


        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally
            dsData.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' UserProfilesのidを取得
    ''' </summary>
    ''' <param name="strUserId"></param>
    ''' <returns></returns>
    Private Function fncGetUserProfileID(ByVal strUserId As String) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim cmd As NpgsqlCommand
        Dim dr As NpgsqlDataReader

        Try
            '---リストボックス表示
            Call subDispList(RET_NORMAL, "ワークキャリア用UserProfileId取得：UserProfiles.UserProFileId = " & strUserId, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "ワークキャリア用UserProfileId取得：UserProfiles.UserProFileId = " & strUserId, strMethod)

            strSQL.Clear()
            strSQL.AppendLine("SELECT ""id""")
            strSQL.AppendLine("FROM ""UserProfiles""")
            strSQL.AppendLine("WHERE ""UserId"" = '" & strUserId & "'")

            '---Postgret接続
            Call ConnectionOpenPostgret()

            '---SQL発行
            cmd = New NpgsqlCommand(strSQL.ToString, connPostgret)
            dr = cmd.ExecuteReader

            '---SQLサーバ切断
            Call ConnectionClosePostgret()

            If dr.HasRows Then
                '---取得データ退避
                While dr.Read()
                    Return dr("id")
                End While
            Else
                Return RET_NOTFOUND
            End If

            dr.Close()

        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally
            dsData.Dispose()
        End Try
    End Function

#End Region

End Class
