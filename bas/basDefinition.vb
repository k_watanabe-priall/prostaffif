﻿Imports System.Data
Imports Oracle.DataAccess.Client
Imports Npgsql
Imports System.Data.SqlClient
Module basDefinition
    ''' <summary>
    ''' 戻り値　正常・・・0
    ''' </summary>
    Public Const RET_NORMAL As Integer = 0
    ''' <summary>
    ''' 戻り値　データなし・・・-1
    ''' </summary>
    Public Const RET_NOTFOUND As Integer = -1
    ''' <summary>
    ''' 戻り値　ワーニング・・・-2
    ''' </summary>
    Public Const RET_WARNING As Integer = -2
    ''' <summary>
    ''' 戻り値　エラー・・・-9
    ''' </summary>
    Public Const RET_ERROR As Integer = -9

    ''' <summary>
    ''' Oracle接続用
    ''' </summary>
    Public connOra As New OracleConnection

    ''' <summary>
    ''' Postgret接続用
    ''' </summary>
    Public connPostgret As New NpgsqlConnection

    ''' <summary>
    ''' SQL Server接続用
    ''' </summary>
    Public connSQLServer As New SqlConnection

    ''' <summary>
    ''' 上位システム用患者情報構造体
    ''' </summary>
    Public Structure TYPE_GET_PATIENT
        Public PATIENTNO As String
        Public SEX As String
        Public BIRTHDAY As String
        Public REFLASTNAME As String
        Public REFFIRSTNAME As String
        Public REFMIDLLENAME As String
        Public KANALASTNAME As String
        Public KANAFIRSTNAME As String
        Public KANAMIDDLENAME As String
        Public KANJILASTNAME As String
        Public KANJIFIRSTNAME As String
        Public KANJIMIDDLENAME As String
    End Structure

    ''' <summary>
    ''' 自DB用患者情報構造体
    ''' </summary>
    Public Structure TYPE_UPD_PATIENT
        Public PATIENTID As String
        Public NAME As String
        Public NAMEKANA As String
        Public BIRTH As String
        Public GENDERID As String
    End Structure

    ''' <summary>
    ''' 上位システム用職員情報構造体
    ''' </summary>
    Public Structure TYPE_GET_STAFF
        Public HOSPITALCODE As String
        Public STAFFCODE As String
        Public GENERATIONNO As String
        Public WARDCODE As String
        Public PROFESSIONCODE As String
        Public DEGREECODE As String
        Public KANANAME As String
        Public KANJINAME As String
        Public PASSWORD As String
        Public VALIDSTARTDATE As String
        Public VALIDENDDATE As String
        Public LICENSECODE1 As String
        Public LICENSEDATE1 As String
        Public LICENSECODE2 As String
        Public LICENSEDATE2 As String
        Public SPECIALFLG As String
        Public PWDUPDATEDATE As String
        Public ENCRYPT As String
        Public ADVISINGDOCTOR As String
        Public SELECTSTAFFSORTKEY As String
        Public WINDOWSUSERNAME As String
    End Structure

    ''' <summary>
    ''' 自DB用職員情報構造体
    ''' </summary>
    Public Structure TYPE_UPD_STAFF
        Public id As String
        Public userid As String
        Public password As String
        Public name As String
        Public nameKana As String
        Public disconnectedAt As String
        Public passwordFailed As String
        Public passwordFailedAt As String
        Public passwordChangedAt As String
        Public MenuId As String
        Public MenuTopId As String
    End Structure

    ''' <summary>
    ''' 職員プロファイル情報構造体
    ''' </summary>
    Public Structure TYPE_UPD_PROFILE
        Public id As String
        Public birth As String
        Public empcode As String
        Public groupcode As String
        Public sectioncode As String
        Public UserId As String
        Public OccupationId As String
        Public PositionId As String
        Public EmpTypeId As String
    End Structure

    ''' <summary>
    ''' 職員ワークキャリア情報構造体
    ''' </summary>
    Public Structure TYPE_UPD_WORKCAREER
        Public id As String
        Public started As String
        Public ended As String
        Public isMain As String
        Public workTime As String
        Public workWeekDay As String
        Public note As String
        Public createdAt As String
        Public updatedAt As String
        Public GroupId As String
        Public UserProfileId As String
        Public OccupationId As String
        Public PositionId As String
        Public EmpTypeId As String
    End Structure

End Module
